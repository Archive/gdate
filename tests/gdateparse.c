
#include <gdate.h>

#include <stdio.h>
#include <string.h>
#include <locale.h>

void g_date_debug_print(GDate* d)
{
  if (!d) g_print("NULL!\n");
  else 
    g_print("julian: %u (%s) MDY: %u %u %u (%s)\n",
	    d->julian_days, 
	    d->julian ? "valid" : "invalid",
	    d->month,
	    d->day,
	    d->year,
	    d->mdy ? "valid" : "invalid");
  
  fflush(stdout);
}

int main(int argc, char** argv)
{
  GDate* d;
  gchar* loc;
  gchar input[1024];

  loc = setlocale(LC_ALL,"");
  if (loc) 
    g_print("\nLocale set to %s\n", loc);
  else 
    g_print("\nLocale unchanged\n");

  d = g_date_new();

  while (fgets(input, 1023, stdin))
    {
      if (input[0] == '\n') 
        {
          g_print("Enter a date to parse and press enter:\n");
          continue;
        }

      g_date_set_parse(d, input);
      
      if (!g_date_valid(d))
        {
          g_print("That looked like nonsense to me!\n");
        }
      else 
        {
          gchar buf[256];
          
          g_date_strftime(buf,100,"Parsed: `%x'\n",
                          d);
          g_print("%s", buf);
        }
    }

  g_date_free(d);

  return 0;
}


